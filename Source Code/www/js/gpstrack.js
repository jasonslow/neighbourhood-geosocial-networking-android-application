function gpstrack(){
	document.addEventListener("deviceready", function(){
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1;
		if(dd<10){dd='0'+dd}
		if(mm<10){mm='0'+mm}
		today = dd + '/' + mm;
		var sesdate = sessionStorage.getItem("trackdate");
		if(sessionStorage.getItem("status") == "Temporarily"){
		if (sesdate !== today || typeof sesdate == 'undefined' || sesdate == null) {
            navigator.geolocation.getCurrentPosition(function(position){
					var lats = position.coords.latitude;
					var longs = position.coords.longitude;
					var dis = dbscanhaversine_dist (lats, longs, sessionStorage.getItem("lat"), sessionStorage.getItem("lon"));
					if (dis <= 0.1){
						savetrack(sessionStorage.getItem("userid"),lats,longs);
					}
				}, function(error){
					if(error.code == PositionError.PERMISSION_DENIED)
					{
						alert("App doesn't have permission to use GPS");
					}
					else if(error.code == PositionError.POSITION_UNAVAILABLE)
					{
						alert("No GPS device found");
					}
					else if(error.code == PositionError.TIMEOUT)
					{
						alert("Its taking too long find user location");
					}
					else
					{
						alert("An unknown error occured");
					}
				}, { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true });
		    }
			}
        }, false);
}

function dbscanhaversine_dist (point1lat, point1lon, point2lat, point2lon)
{
 // default 4 sig figs reflects typical 0.3% accuracy of spherical model
	if (typeof precision === 'undefined') {
		var precision = 4;
	}

	var R = 6371;
	var lat1 = point1lat * Math.PI / 180,
		lon1 = point1lon * Math.PI / 180;
	var lat2 = point2lat * Math.PI / 180,
		lon2 = point2lon * Math.PI / 180;

	var dLat = lat2 - lat1;
	var dLon = lon2 - lon1;

	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		Math.cos(lat1) * Math.cos(lat2) *
		Math.sin(dLon / 2) * Math.sin(dLon / 2);

	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c;

	return d.toPrecision(precision);
}

function savetrack(userid,lats,longs){
	$.ajax({
        type: "POST",
        url: "http://smartgreen.my/[TARISA]/class session/geosocial/track.php",
        data: { userid: userid, lats: lats, longs: longs},
        cache: false,  
        dataType: 'json',
        success: function(html){
            sessionStorage.setItem("trackdate",today);
        },
        error: function (e) {
                console.log(e);
        }/*,
        error: function (jQXHR, textStatus, errorThrown) {
                alert("An error occurred whilst trying to contact the server: " + jQXHR.status + " " + textStatus + " " + errorThrown);
        }  */                                   
    });
}