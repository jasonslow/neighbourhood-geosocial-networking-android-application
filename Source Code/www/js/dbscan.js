function dbscanrun (points, num_points, epsilon, minpts)
             {
         		var i, cluster_id = 0;
                 for (i = 0; i < num_points; ++i) {
                     if (points[i].cluster_id === UNCLASSIFIED) {
                         if (expand(i, cluster_id, points,
                             num_points, epsilon, minpts
                             ) === CORE_POINT)
                             ++cluster_id;
                     }
                 }
             }
         	
         	//Second run function
         	
             function expand(index, cluster_id, points,
                 num_points, epsilon, minpts)
             {
                 var i, return_value = NOT_CORE_POINT,
                     seeds = get_epsilon_neighbours(index, points,
                         num_points, epsilon);
                 if (seeds.length < minpts)
                     points[index].cluster_id = NOISE;
                 else {
                     points[index].cluster_id = cluster_id;
                     for (i = 0; i < seeds.length; ++i)
                         points[seeds[i]].cluster_id = cluster_id;
                     for (i = 0; i < seeds.length; ++i)
                         spread(seeds[i], seeds, cluster_id, points,
                             num_points, epsilon, minpts);
                     return_value = CORE_POINT;
                 }
                 return return_value;
             }
         
         	//Third run function
         	
             function get_epsilon_neighbours(index, points, num_points, epsilon)
             {
                 var i, d;
         		var en = []; 
                 for (i = 0; i < num_points; ++i) {
                     if (i === index)
                         continue;
                     d = dbscanhaversine_dist(points[index], points[i]);
                     if (d > epsilon)
                         continue;
                     else
                         en.push(i);
                 }
                 return en;
             }
         
             function spread(index, seeds, cluster_id, points,
                 num_points, epsilon, minpts)
             {
                 var i, c, d, idx,
                     spread = get_epsilon_neighbours(index, points,
                         num_points, epsilon);
                 c = spread.length;
                 if (c >= minpts) {
                     for (i = 0; i < c; ++i) {
                         idx = spread[i];
                         d = points[idx];
                         if (d.cluster_id === NOISE ||
                             d.cluster_id === UNCLASSIFIED) {
                             if (d.cluster_id === UNCLASSIFIED) {
                                 seeds.push(idx);
                             }
                             d.cluster_id = cluster_id;
                         }
                     }
                 }
                 return SUCCESS;
             }
         
             function dbscanhaversine_dist (point1, point2)
             {
                 // default 4 sig figs reflects typical 0.3% accuracy of spherical model
         			if (typeof precision === 'undefined') {
         				var precision = 4;
         			}
         
         			var R = 6371;
         			var lat1 = point1.lat * Math.PI / 180,
         				lon1 = point1.lon * Math.PI / 180;
         			var lat2 = point2.lat * Math.PI / 180,
         				lon2 = point2.lon * Math.PI / 180;
         
         			var dLat = lat2 - lat1;
         			var dLon = lon2 - lon1;
         
         			var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
         				Math.cos(lat1) * Math.cos(lat2) *
         				Math.sin(dLon / 2) * Math.sin(dLon / 2);
         
         			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
         			var d = R * c;
         
         			return d.toPrecision(precision);
             }
             
             // This count the frequency of people inside the polygon area
             
		             counted = function(ary, classifier) {
		    return ary.reduce(function(counter, item) {
		        var p = (classifier || String)(item);
		        counter[p] = counter.hasOwnProperty(p) ? counter[p] + 1 : 1;
		        return counter;
		    }, {})
		};
		
				Object.size = function(obj) {
		    var size = 0, key;
		    for (key in obj) {
		        if (obj.hasOwnProperty(key)) size++;
		    }
		    return size;
		};
		
		
	//end of function